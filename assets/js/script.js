/*array literal = []
other style is using the word "new Array ('' , '')"

*/

const array1 = ['eat', 'sleep'];
console.log(array1)

const array2 = new Array('pray', 'play');
console.log(array2)

/*empty array*/
const myList = [];

/*array of numbers*/
const numArray = [2, 3, 4, 5,]

/*array of strings*/
const stringArray = ['eat','work', 'pray', 'play'];


const newData1 = [
	{'task1': 'exercise'}, 
	[1,2,3],
	function hello(){
		console.log('Hi I am Array')
	}
];

console.log(newData1);


/*mini activity*/

let activityArray = ['Japan', 'Australia', 'Siargao', 'Boracay', 'South Korea', 'France', 'Spain'];
console.log(activityArray[0]);
console.log(activityArray);
console.log(activityArray[activityArray.length - 1]);
console.log(activityArray.length);

for(let i = 0; i < activityArray.length; i++){
	console.log(activityArray[i]);
};

/*array manipulation*/

/*adds another element at the end to a completed array by using push()*/

let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);

/*unshift() = adds another element of a completed array*/

dailyActivities.unshift('sleep');
console.log(dailyActivities);

dailyActivities[2] = 'sing'
console.log(dailyActivities);

dailyActivities[6] = 'dance'
console.log(dailyActivities);

/*let activityArray = ['Japan', 'Australia', 'Siargao', 'Boracay', 'South Korea', 'France', 'Spain'];
console.log(activityArray[0]);
console.log(activityArray);
console.log(activityArray[activityArray.length - 1]);
console.log(activityArray.length);

for(let i = 0; i < activityArray.length; i++){
	console.log(activityArray[i]);
};*/

activityArray[3] = 'Giza Sphinx'
console.log(activityArray);

console.log(activityArray[5]);
activityArray[5] = 'Turkey';
console.log(activityArray);
console.log(activityArray[5]);

/*mini activity*/
activityArray[0] = 'Iloilo City'
activityArray[6] = 'INHS-SSC'
console.log(activityArray);
console.log(activityArray[0]);
console.log(activityArray[6]);



/*adding items without using methods*/

let array11 = [];
console.log(array11[0]);
array11[0] = 'Cloud Strife';
console.log(array11);
console.log(array11[1]);
array11[1] = 'Tifa Lockhart';
console.log(array11[1]);
array11[array11.length-1] = 'Aerith Gainsborough'
console.log(array11);
array11[array11.length] = 'Vincent Valentine'
console.log(array11);

/*Array Methods*/


let array21 = ['Juan', 'Pedro', 'Jose', 'Andres'];

array21[array21.length] = 'Francisco';
console.log(array21);

array21.push('Andres');
console.log(array21);

array21.unshift('Simon');
console.log(array21);

/*.pop() - allows us to delete or remove the last item/element of the array*/

array21.pop();
console.log(array21);

console.log(array21.pop()); /*to restore the deleted item (last element)*/
console.log(array21);

let removedItem = array21.pop();
console.log(array21);
console.log(removedItem);


/*.shift() returns the item we removed or deletes the first element*/
let removedItemShift = array21.shift();
console.log(array21);
console.log(removedItemShift);


/*mini activity*/

array11.shift();
console.log(array11);

array11.pop();
console.log(array11);

array11.unshift('George');
console.log(array11);

array11.push('Michael');
console.log(array11);

/*.sort() - by default, it sorts the items in ascending order*/

array11.sort()
console.log(array11);

let numArray1 = [3, 2, 1, 6, 7, 9];
numArray1.sort();
console.log(numArray1);

let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
numArray2.sort((a,b)=> a-b);
console.log(numArray2);

/*.sort() converts all items INTO STRINGS (like words/texts) before sorting*/


numArray2.sort(function(a,b){
	return a-b
})
console.log(numArray2) /*long method of line 162 for ascending*/

numArray2.sort(function(a,b){
	return b-a
})
console.log(numArray2) /*long method of line 162 for descending*/

let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine']
arrayStr.sort(function(a,b){
	return b-a
})

console.log(arrayStr);

/*.reverse() - reverses the order of items*/


arrayStr.sort();
console.log(arrayStr);

arrayStr.sort().reverse();
console.log(arrayStr);


/*mutators*/

/*splice() - allows us to remove and add elements from a given index
array.splice(startingIndex, numberOfItemsToBeDeleted, elementsToAdd)
*/
let beatles = ['George', 'John', 'Paul', 'Ringo']
let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq']


lakersPlayers.splice(0, 0, 'Caruso');
console.log(lakersPlayers)

lakersPlayers.splice(0, 1);
console.log(lakersPlayers)

lakersPlayers.splice(0, 3,);
console.log(lakersPlayers)

lakersPlayers.splice(1, 1);
console.log(lakersPlayers)

lakersPlayers.splice(1, 0, 'Gasol', 'Fisher');
console.log(lakersPlayers)


/*non mutators - will not change the original array*/

/*slice() - allows us to get a portion of the original array and return a new array with the items selected from the original*/

/*syntax
slice(startIndex, endIndex);

*/

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];

computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer')
console.log(computerBrands);

let newBrands = computerBrands.slice(1, 3);
console.log(computerBrands);
console.log(newBrands);

let fonts = ['Times New Roman', 'Comic Sans MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];

let newFontSet = fonts.slice(1, 4);
console.log(newFontSet);

newFontSet = fonts.slice(2);
console.log(newFontSet);



/*mini activity*/

let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1']

let microsoft = videoGame.slice(3)
console.log(microsoft);
let nintendo = videoGame.slice(2, 4)
console.log(nintendo);

/*.toString() - convert the array into a single value but each item will be separated by a comma. */


let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun', '.'];
let sentenceString = sentence.toString();

console.log(sentence);
console.log(sentenceString);

/*.join() - converts the array into a single value as a string buy seperator can be specified*/

let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join(' ');

console.log(sentence2);

let sentenceString3 = sentence2.join("");
console.log(sentenceString3);



/*october 8 mini activity*/

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];


let charArr1 = charArr.slice(5, 11);
let charArr2 = charArr.slice(13,19);
console.log(charArr1);
console.log(charArr2);

let name1 = charArr1.join('');
let name2 = charArr2.join('');

console.log(name1);
console.log(name2);


/*.concat() - combines 2 or more arrays*/

let tasksFriday = ['drink HTML', ' eat JavaScript'];
let tasksSaturday = ['inhale CSS', 'breath BootStrap'];

let tasksSunday = ['Get Git', 'Be Node']

let weekendTask = tasksFriday.concat(tasksSaturday, tasksSunday)

console.log(weekendTask);


/*assessors

method that allows us to access our array. */

let batch131 = ['Paolo', 'Jamir', 'Jed',  'Ronel', 'Rom', 'Jayson'];

console.log(batch131.indexOf('Jed'));
console.log(batch131.indexOf('Rom'));


/*last indexOf
	finds the index of the given element/item when it is last found from the right
*/


console.log(batch131.lastIndexOf('Jamir'));

/*mini activity*/

let carBrands = ['BMW', 'Dodge', 'Maserati', 'Porsche', 'Chevrolet', 'Ferrari', 'GMC', 'Porsche', 'Mitsubishi', 'Toyota', 'Volkswagen', 'BMW'];


function firstBrand (firstCar) {
	console.log(carBrands.indexOf(firstCar));
}

firstBrand('BMW')

function lastFirstBrand (firstCar) {
	console.log(carBrands.lastIndexOf(firstCar));
}

lastFirstBrand('BMW');

/*iterator method*/

let avengers = ['Hulk', 'Black Widow', 'Hawkeye', 'Spiderman', 'Ironman', 'Captain America'];
/*forEach()
	similar to for Loop but only used on arrays. 
*/

avengers.forEach(function(avenger){
	console.log(avenger);
});



let marvelHeroes = ['Moon Knight', 'Jessica Jones', 'Deadpool', 'Cyclops'];

marvelHeroes.forEach(function(hero){
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		avengers.push(hero);
	}
});
console.log(avengers);

/*map() - similar to forEach but it returns a new array*/

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});

console.log(mappedNumbers);


/*other iterators
	every() - iterates all items and checks if all passes the given condition
	some() - iterates all items and check if at least one passes the condition
	filter() - creates a new array that contains elements which passed the condition
	find() - iterate all items in the array but only returns the item that passed the condition
	.includes() - returns a boolean true if it matches the item from the array
*/

let allMemberAge = [25, 30, 15, 2, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);


let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
	});

console.log(checkForPassing);

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});

console.log(divisibleBy5);


let registereduserNames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCodes'];

let foundUser = registereduserNames.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000';

});

console.log(foundUser);

let registeredEmails = ['johnnyPhoenix1991@gmail.com', 'michaelKing@gmail.com', 'pedro_himself@yahoo.com', 'sheJonesSmith@gmail.com'];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');

console.log(doesEmailExist);

/*mini activity*/


let existingNames = ['karen202', 'macaroni', 'weak', 'heheCoder'];

let existingUser = existingNames.find(function(existingUserName){
	console.log(existingUserName);
	return existingUserName === 'heheCoder';

});


function checkEmail(email){
	let usedEmail = registeredEmails.includes(email);
	if (usedEmail == true){
		alert('Email already exists')
	} else alert('Email is available, proceed to registration')
};

checkEmail('sheJonesSmith@gmail.com');

function checkEmail(email){
	let usedEmail = registeredEmails.includes(email);
	if (usedEmail == true){
		alert('Email already exists')
	} else alert('Email is available, proceed to registration')
};

checkEmail('invisible@gmail.com');